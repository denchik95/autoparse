﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoParse
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private ThreatFileManager fileManager =
			new ThreatFileManager(Directory.GetCurrentDirectory());

		public int ThreatsPerPage = 20;
		public int PageNumber { get; set; }
		public List<Threat> AllThreats { get; private set; } = new List<Threat>();
		public ObservableCollection<Threat> PageThreats { get; } = new ObservableCollection<Threat>();

		public MainWindow()
		{
			InitializeComponent();
			UBIList.ItemsSource = PageThreats;

			Task.Run(LoadThreatsToView);
		}

		private async Task LoadThreatsToView()
		{
			if (!fileManager.IsFileExist())
			{
				bool isDownloaded = await ConfigureFirstStart();
				if (!isDownloaded) return;
			}

			try
			{
				AllThreats = fileManager.LoadAllThreatsFromFile();
				SetPage(0);
			}

			catch (Exception exception)
			{
				MessageBox.Show(exception.Message);
			}
		}

		private async Task<bool> ConfigureFirstStart()
		{
			MessageBoxResult result = MessageBox.Show("Файла в локальной базе не существует. " +
				"Хотите выполнить первичную загрузку?",
				"Оповещение", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
			if (result != MessageBoxResult.Yes) return false;

			await fileManager.DownloadFile();
			return true;
		}

		private void SetPage(int pageNumber)
		{
			PageNumber = pageNumber;
			int skipCount = pageNumber * ThreatsPerPage;

			Dispatcher.Invoke(PageThreats.Clear);
			foreach (Threat newTreat in AllThreats.Skip(skipCount).Take(ThreatsPerPage))
				Dispatcher.Invoke(() => PageThreats.Add(newTreat));
		}

		private async void Export_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog()
			{
				DefaultExt = "txt",
				Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"
			};

			bool? result = saveFileDialog.ShowDialog();

			if (result != true) return;

			try
			{
				string export = "";
				foreach (Threat threat in AllThreats)
				{
					export +=
						$"{threat.ID}\t{threat.NameOfThreat}\t{threat.Specification}\t{threat.SourceOfThreat}\t{threat.ObjectOfInfluence}\t" +
						$"{threat.Confidentiality}\t{threat.Integrity}\t{threat.Availability}\n";
				}

				byte[] bytes = Encoding.UTF8.GetBytes(export);
				using (Stream stream = saveFileDialog.OpenFile())
				{
					await stream.WriteAsync(bytes, 0, bytes.Length);
				}

				MessageBox.Show("Файл сохранен","Оповещение",MessageBoxButton.OK,MessageBoxImage.Information);
			}
			catch (Exception exception)
			{
				MessageBox.Show(exception.Message);
			}
		}

		private async void Update_Click(object sender, RoutedEventArgs e)
		{
            try
            {
                Button button = (Button)sender;
               

                await fileManager.DownloadFile();
                List<Threat> newList = fileManager.LoadAllThreatsFromFile();

                CompareToTreatsVersions(AllThreats, newList);

                AllThreats = newList;
                SetPage(0);

                
               
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
		}

		private void PrevPage_OnClick(object sender, RoutedEventArgs e)
		{
			if (PageNumber < 1) return;
			SetPage(PageNumber - 1);
			PageNumberLabel.Content = PageNumber + 1;
		}

		private void NextPage_OnClick(object sender, RoutedEventArgs e)
		{
			if (PageNumber + 1 > AllThreats.Count / ThreatsPerPage) return;
			SetPage(PageNumber + 1);
			PageNumberLabel.Content = PageNumber + 1;
		}

		private void UBIList_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (UBIList.SelectedItem != null && UBIList.SelectedItem is Threat threat)
			{
				ThreatInfo infoWindow = new ThreatInfo(threat);
				infoWindow.Show();
			}
		}

		private void CompareToTreatsVersions(List<Threat> was, List<Threat> now)
		{
			IEnumerable<Threat> deleted = was.Where(wasThreat => now.All(x => x.ID != wasThreat.ID));
			IEnumerable<CompareThreatPair> deletedPair = deleted.Select(x => new CompareThreatPair() {Was = x, Now = null});

			IEnumerable<Threat> added = now.Where(nowThreat => was.All(x => x.ID != nowThreat.ID));
			IEnumerable<CompareThreatPair> addedPair = added.Select(x => new CompareThreatPair() { Was = null, Now = x });

			List<CompareThreatPair> changedPair = new List<CompareThreatPair>();
			foreach (Threat wasThreat in was)
			{
				Threat newVersion = now.FirstOrDefault(x => x.ID == wasThreat.ID);
				if (newVersion == null) continue;
                if (wasThreat.Equals(newVersion)) continue;
				
				changedPair.Add(new CompareThreatPair() {Was = wasThreat, Now = newVersion});
			}

			List<CompareThreatPair> allChanges = deletedPair.Concat(changedPair).Concat(addedPair).ToList();

			if (!allChanges.Any())
			{
				MessageBox.Show("Изменений не обнаружено","Оповещение",MessageBoxButton.OK,MessageBoxImage.Information);
			}
			else
			{
				new UpdateNotesWindow(allChanges.ToList()).Show();
                int allChangesCount =allChanges.Count;
                MessageBox.Show($"Обновление прошло успешно, количество обновленных записей: {allChangesCount}", "Оповещение", MessageBoxButton.OK, MessageBoxImage.Information);
            }
		}
	}

}
