﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoParse
{
	/// <summary>
	/// Interaction logic for UpdateNotesWindow.xaml
	/// </summary>
	public partial class UpdateNotesWindow : Window
	{
		public UpdateNotesWindow(List<CompareThreatPair> notes)
		{
			InitializeComponent();
			NotesList.ItemsSource = new ObservableCollection<CompareThreatPair>(notes);
		}
	}
}
