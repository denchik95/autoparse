﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoParse
{
	/// <summary>
	/// Interaction logic for ThreatInfo.xaml
	/// </summary>
	public partial class ThreatInfo : Window
	{
		public ThreatInfo(Threat threat)
		{
			InitializeComponent();

			ID.Text = threat.ID;
			NameOfThreat.Text = threat.NameOfThreat;
			Specification.Text = threat.Specification;
			SourceOfThreat.Text = threat.SourceOfThreat;
			ObjectOfInfluence.Text = threat.ObjectOfInfluence;
			Confidentiality.Text = threat.Confidentiality;
			Integrity.Text = threat.Integrity;
			Availability.Text = threat.Availability;
		}
	}
}
