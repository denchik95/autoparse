﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AutoParse
{
	public class CompareThreatPair
	{
		public Threat Was { get; set; }
		public Threat Now { get; set; }
	}
}
