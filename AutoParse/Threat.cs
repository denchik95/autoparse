﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kasay;

namespace AutoParse
{
    public class Threat
    {
         public string ID { get; set; }
         public string NameOfThreat { get; set; }
         public string Specification { get; set; }
         public string SourceOfThreat { get; set; }
         public string ObjectOfInfluence { get; set; }
         public string Confidentiality { get; set; }
         public string Integrity { get; set; }
         public string Availability { get; set; }

        public override bool Equals(object obj)
        {
	        if (obj is Threat other)
		        return Equals(other);

	        return false;
        }

        protected bool Equals(Threat other)
        {
	        return ID == other.ID && NameOfThreat == other.NameOfThreat &&
                Specification == other.Specification && SourceOfThreat == other.SourceOfThreat &&
                ObjectOfInfluence == other.ObjectOfInfluence && 
                Confidentiality == other.Confidentiality && Integrity == other.Integrity && 
                Availability == other.Availability;
        }

        public override int GetHashCode()
        {
	        unchecked
	        {
		        int hashCode = (ID != null ? ID.GetHashCode() : 0);
		        hashCode = (hashCode * 397) ^ (NameOfThreat != null ? NameOfThreat.GetHashCode() : 0);
		        hashCode = (hashCode * 397) ^ (Specification != null ? Specification.GetHashCode() : 0);
		        hashCode = (hashCode * 397) ^ (SourceOfThreat != null ? SourceOfThreat.GetHashCode() : 0);
		        hashCode = (hashCode * 397) ^ (ObjectOfInfluence != null ? ObjectOfInfluence.GetHashCode() : 0);
		        hashCode = (hashCode * 397) ^ (Confidentiality != null ? Confidentiality.GetHashCode() : 0);
		        hashCode = (hashCode * 397) ^ (Integrity != null ? Integrity.GetHashCode() : 0);
		        hashCode = (hashCode * 397) ^ (Availability != null ? Availability.GetHashCode() : 0);
		        return hashCode;
	        }
        }
    }
}
