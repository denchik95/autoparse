﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AutoParse
{
	public class ThreatFileManager
	{
		private static string fileName = "thrlist.xlsx";
		private static Uri threatFileUri = new Uri("https://bdu.fstec.ru/documents/files/" + fileName);
		public string FilePath { get; private set; }

		public ThreatFileManager(string path)
		{
			FilePath = path + "\\" + fileName; 
		}

		public bool IsFileExist()
		{
			return File.Exists(FilePath);
		}

		public async Task DownloadFile()
		{
			WebClient web = new WebClient();
			await web.DownloadFileTaskAsync(threatFileUri, FilePath);
		}

		private FileStream GetFileStream()
		{
			if (!IsFileExist())
				throw new Exception("Файл не найден");

			return File.OpenRead(FilePath);
		}

		public List<Threat> LoadAllThreatsFromFile()
		{
			List<Threat> threats = new List<Threat>();
			using (FileStream baseStream = GetFileStream())
			{
				using (ExcelPackage excelPackage = new ExcelPackage(baseStream))
				{
					ExcelWorksheet firstList = excelPackage.Workbook.Worksheets[1];
					int lastRow = firstList.Dimension.End.Row;

					for (int row = 3; row <= lastRow; row++)
					{
						string id = firstList.Cells[row, 1].Value?.ToString().Replace("_x000d_","\r");
						string nameOfThreat = firstList.Cells[row, 2].Value?.ToString().Replace("_x000d_", "\r");
						string specification = firstList.Cells[row, 3].Value?.ToString().Replace("_x000d_", "\r");
						string sourceOfThreat = firstList.Cells[row, 4].Value?.ToString().Replace("_x000d_", "\r");
						string objectOfInfluence = firstList.Cells[row, 5].Value?.ToString().Replace("_x000d_", "\r");
						string confidentiality = firstList.Cells[row, 6].Value?.ToString().Replace("_x000d_", "\r");
						string integrity = firstList.Cells[row, 7].Value?.ToString().Replace("_x000d_", "\r");
						string availability = firstList.Cells[row, 8].Value?.ToString().Replace("_x000d_", "\r");

                        if (confidentiality == "0") confidentiality = "Нет";
                        else if (confidentiality == "1") confidentiality = "Да";
                        if (integrity == "0") integrity = "Нет";
                        else if (integrity == "1") integrity = "Да";
                        if (availability == "0") availability = "Нет";
                        else if (availability == "1") availability = "Да";

                       





                    if (new []  {id, nameOfThreat,specification,sourceOfThreat,objectOfInfluence,
							confidentiality,integrity,availability}.Any(x => x == null))
							continue;

						threats.Add(new Threat()
						{
							ID = id,
							NameOfThreat = nameOfThreat,
							Specification = specification,
							SourceOfThreat = sourceOfThreat,
							ObjectOfInfluence = objectOfInfluence,
							Confidentiality = confidentiality,
							Integrity = integrity,
							Availability = availability
						});
					}
				}

			}

			return threats;
		}
	}
}
